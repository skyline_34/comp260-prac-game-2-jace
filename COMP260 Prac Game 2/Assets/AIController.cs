﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class AIController : MonoBehaviour {
    private Rigidbody rigidbody;
    public float speed = 20f;
    public Transform puck;
    public Transform goal;
    public Transform defGoal;
    public bool onRoad;
    public bool finishHit;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (puck.position.x > 0)
        {
            onRoad = false;
        }
        else
        {
            onRoad = true;
        }
        if (!onRoad)
        {
            if (rigidbody.position.x >3)
            {
                Vector3 temp = puck.position - goal.position;
                temp = temp.normalized;
                Vector3 pos = puck.position + temp*0.2f;
                Vector3 dir = pos - rigidbody.position;
                Vector3 vel = dir.normalized * speed;
                // check is this speed is going to overshoot the target
                float move = speed * Time.fixedDeltaTime;
                float distToTarget = dir.magnitude;
                if (move > distToTarget)
                {
                    // scale the velocity down appropriately
                    vel = vel * distToTarget / move;
                }
                rigidbody.velocity = vel;
                
                onRoad = true;
            }
            else
            {
                Vector3 dir2 = new Vector3(1, 0, 0);
                dir2 = dir2.normalized;
                rigidbody.velocity = dir2*speed;
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
    void OnCollisionEnter(Collision collision)
    {
        this.onRoad = false;
    }
    public void stopSelf()
    {
        if (rigidbody)
        {
            rigidbody.position = new Vector3(3.8f, 0, 0);
            rigidbody.velocity = Vector3.zero;
            this.onRoad = true;
        }
    }
}
