﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class PuckControl : MonoBehaviour {

   // private Vector3 Pose =new Vector3(0f,0f,0f);
    public Transform startingPos;
    private Rigidbody rigidbody;
    private AudioSource audio;
    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;
    public AIController smart;
    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ResetPosition()
    {

        Debug.Log("initial position"+rigidbody.position);
        Vector3 Pose = new Vector3(0f, 0f, 0f);
        // teleport to the starting position
        Debug.Log(Pose);
        // rigidbody.MovePosition(startingPos.position);
        rigidbody.velocity = Vector3.zero;
        rigidbody.position = Pose;
        // stop it from moving
        smart.stopSelf();
        
        
    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Collision Enter: " + collision.gameObject.name);
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            audio.PlayOneShot(paddleCollideClip);
            smart.onRoad = false;
        }
        else
        {
            // hit something else
            audio.PlayOneShot(wallCollideClip);
        }

    }
    void OnCollisionStay(Collision collision)
    {
        Debug.Log("Collision Stay: " + collision.gameObject.name);
    }
    void OnCollisionExit(Collision collision)
    {
        Debug.Log("Collision Exit: " + collision.gameObject.name);
    }

}
